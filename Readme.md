![Crowd Mics logo](https://bytebucket.org/crowdmics/cmmanager/raw/a1affaf1715159acf9a7b121fce84d7f7284e7b8/Crowd%20Mics%20Logo%20Transparent%20Wide%20Black-1.png)

##Introduction
The Crowd Mics SDK is available for Crowd Mics partners that wish to add **the microphone of the future** to their existing iOS app. This allows you to connect to Crowd Mics events that are already running on the same WiFi network (requires Crowd Mics app to be running in presenter mode), request to talk, and use the microphone. You must have a valid license to use the SDK.

The SDK is currently distributed through CocoaPods.


##Getting Started
You will need to add the following lines to your `Podfile`:

```
source 'https://bitbucket.org/crowdmics/legacy-pods-specs.git'

pod 'CMManager', '~> 0.0.23'
```

Then run `pod install` to have the dependencies added to your project.

##Licensing

**Before using the SDK, you will need to add a new string value to your plist file**:

`Key`: *CMLicenseKey*

`Value`: *Your license key*

##Using CMManager
CMManager exists as a singleton. You can access the entirety of the SDK by using methods that are available through `[CMManager sharedSession]`. Multiple delegates are available depending on your task.

###CMManager and Licensing
Because you must have a valid license that must be checked upon the first usage of the SDK, `CMManager` provides a delegate `CMManagerLicenseDelegate` to allow you to handle ensuring that your license check passed and handling problems that may arise. **You should always check that your license check has succeeded when first using the SDK by setting the license delegate (as shown in the sample application) to avoid confusion with your users**.

```
- (void)licenseVerificationSucceeded; // Called when license verification succeeds
- (void)licenseVerificationError; // Called when there is an error in license verification (such as being unable to reach the server)
- (void)licenseVerificationFailed; // Called when the license being used is invalid
```

##Finding & Connecting to Events
Crowd Mics discovery ***currently*** only works through Bonjour. You must have Bonjour and peer-to-peer networking enabled on your network (and the Crowd Mics presenter app running) to discover and connect to events.

Start event discovery:

```
[[CMManager sharedSession] setDiscoveryDelegate:self];
[[CMManager sharedSession] startEventDiscovery];
```
When an event is found:

```
- (void)didFindEvent:(NSDictionary *)event {
    NSLog(@"did find event : %@", event);
}

```
An event may be found multiple times.

You may then connect to an event that was discovered:

```
[[CMManager sharedSession]connectToEventWithUserFirstName:@"SDK" lastName:@"User"
                                                             uuid:[[NSUUID UUID] UUIDString]
                                                         passcode:@"123" event];
```

If your event doesn't require a passcode:

```
[[CMManager sharedSession]connectToEventWithUserFirstName:@"SDK" lastName:@"User"
                                                             uuid:[[NSUUID UUID] UUIDString]
                                                         andEvent:selectedEvent];

```

##Request to Talk
Once connected to an event, you can request to talk. 
```
[[CMManager sharedSession] requestToTalk];
```

To cancel the request:

```
[[CMManager sharedSession] stopRequestToTalk];
```

Once selected to speak (from the presenter), the `CMManagerEventTalkDelegate` will be called and we can start the microphone:

```
- (void)didReceiveAcceptTalkRequest:(id<CMClientTalkService>)clientTalkService {
    // We can now speak
    [[CMManager sharedSession] speak];
}
```



Other delegate methods:

```
- (void)didSendRequestToTalk:(CMControlServerResponse*)response;
- (void)didReceiveRejectTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveBlockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveUnblockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveAcceptTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveFinishTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)didCancelRequestToTalk;
```

##Sample Project
A sample project that uses the SDK is available at <https://bitbucket.org/crowdmics/crowdmics-sdk-sample>

**You will need a valid license to use the SDK.**
