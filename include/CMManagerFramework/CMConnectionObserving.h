//
//  CMConnectionObserving.h
//  CrowdMics
//
//  Created by Max Mashkov on 6/7/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppleGuice/AppleGuice.h>
#import "AppleGuice.h"

@class CMControlConnection;
@protocol CMControlServiceDelegate;

@protocol CMConnectionCloseObserverDelegate <NSObject>

@optional
- (void)connectionDidEstablish:(CMControlConnection *)connection;
- (void)connectionDidClose:(CMControlConnection *)connection withError:(NSError *)error;

@end

@protocol CMConnectionObserving <NSObject, CMControlServiceDelegate, AppleGuiceInjectable, AppleGuiceSingleton>

- (void)addDelegate:(id<CMConnectionCloseObserverDelegate>)delegate;
- (void)removeDelegate:(id<CMConnectionCloseObserverDelegate>)delegate;

@end
