//
// Created by zen on 6/5/13.
// Copyright (c) 2013 111Minutes. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "CMConnectionObserving.h"

@protocol CMConnectionAdapter;
@protocol CMControlServiceProtocol;
@class CMEvent;


@interface CMProtocolController : NSObject

@property (nonatomic, strong, readonly) id<CMConnectionAdapter> connectionAdapter;
@property (nonatomic, strong, readonly) id<CMControlServiceProtocol> controlService;
@property (nonatomic, strong, readonly) CMEvent *event;
@property (nonatomic, strong, readonly) id<CMConnectionObserving> ioc_connectionObserver;

- (id)initWithEvent:(CMEvent *)event name:(NSString *)name;
- (void)start;
- (void)disconnect;
- (id <CMControlServiceProtocol>)createControlServiceWithName:(NSString *)name;
@end