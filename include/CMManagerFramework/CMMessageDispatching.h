//
// Created by zen on 6/3/13.
// Copyright (c) 2013 111Minutes. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class CMControlMessage;
@class CMControlClientRequest;
@class CMControlServerResponse;
@class CMControlConnection;
@class CMControlService;

typedef void (^CMMessageResponseHandler)(CMControlServerResponse *response);
typedef void (^CMMessageDispatcherHandler)(CMControlMessage *message, CMControlConnection *connection);
typedef void (^CMMessageDispatcherResponseHandler)(CMControlServerResponse *response, CMControlConnection *connection);

@protocol CMMessageDispatching <NSObject>

- (void)addResponseHandlerForRequest:(CMControlClientRequest *)request withBlock:(CMMessageDispatcherResponseHandler)block;
- (void)addHandlerForMessageClass:(Class)requestClass withBlock:(CMMessageDispatcherHandler)block;

- (BOOL)dispatchMessage:(CMControlMessage *)message fromConnection:(CMControlConnection *)connection;

@end