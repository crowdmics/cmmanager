//
//  CMEvent.h
//  CrowdMicsPrototype
//
//  Created by Maxim on 3/13/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMDomainModel.h"
#import "CMUser.h"

typedef enum {
    EventModeSelect,
    EventModeOpenMic,
} EventStateMode;

@interface CMEvent : CMDomainModel

@property (nonatomic, copy) NSString *uuid;
@property (nonatomic, copy) NSString *passcodeString;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL hasPasscode;
@property (nonatomic, assign) BOOL areCommentsEnabled;
@property (nonatomic, assign) BOOL areMicsEnabled;
@property (nonatomic, assign) EventStateMode mode;

@property (nonatomic, copy) NSString *host;
@property (nonatomic, assign) NSInteger port;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) UIImage *sponsorImage;
@property (nonatomic, copy) NSString *dedicatedHardwareAddress;



@end

