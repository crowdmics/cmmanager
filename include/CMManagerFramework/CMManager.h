//
//  CMManager.h
//  CMEventDiscoveryAndConnectionFramework
//
//  Created by Blake Carrier on 8/24/15.
//  Copyright © 2015 Blake Carrier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMClientProtocolController.h"
#import "CMClientTalkController.h"

@protocol CMManagerEventDiscoveryDelegate;
@protocol CMManagerEventTalkDelegate;
@protocol CMManagerLicenseDelegate;


@interface CMManager : NSObject
@property (nonatomic, assign) id <CMManagerEventDiscoveryDelegate> discoveryDelegate;
@property (nonatomic, assign) id <CMManagerEventTalkDelegate> talkDelegate;
@property (nonatomic, assign) id <CMManagerLicenseDelegate> licenseDelegate;

+ (CMManager *)sharedSession;

- (void)startEventDiscovery;

- (void)connectToEventWithUserFirstName:(NSString *)firstName lastName:(NSString *)lastName
                                   uuid:(NSString *)uuid passcode:(NSString *)passcode andEvent:(id)event;

- (void)connectToEventWithUserFirstName:(NSString *)firstName lastName:(NSString *)lastName
                                   uuid:(NSString *)uuid
                               andEvent:(id)event;

- (void)requestToTalk;

- (void)stopRequestToTalk;

- (void)speak;

- (void)stopSpeaking;

- (BOOL)isConnected;

- (BOOL)isAbleToSpeak;

- (BOOL)isVerifyingLicense;

- (NSDictionary *)currentlyConnectedEvent;
@end

/*
 Crowd Mics Manager Delegate
 */

@protocol CMManagerLicenseDelegate <NSObject>
- (void)licenseVerificationSucceeded;
- (void)licenseVerificationError;
- (void)licenseVerificationFailed;

@end

@protocol CMManagerEventDiscoveryDelegate <NSObject>

@required
- (void)didFindEvent:(NSDictionary *)event;
- (void)didRemoveEvent:(NSDictionary *)event;
- (void)didConnectToEvent:(CMEvent *)event withClientController:(CMClientProtocolController *)clientController;
- (void)connectionToEventFailed:(CMControlServerResponse *)response;
- (void)connectionToEventTimedOut;

@end


@protocol CMManagerEventTalkDelegate <NSObject>

- (void)didSendRequestToTalk:(CMControlServerResponse*)response;
- (void)didReceiveRejectTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveBlockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveUnblockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveAcceptTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)didReceiveFinishTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)didCancelRequestToTalk;
- (void)didReceiveStopEventMessage;

@end