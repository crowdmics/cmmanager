//
//  CMClientTalkService.h
//  CrowdMics
//
//  Created by Max Mashkov on 6/12/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMMessageDispatching.h"
#import "CMEvent.h"

@protocol CMConnectionAdapter;
@protocol CMClientTalkService;

@protocol CMClientTalkServiceDelegate <NSObject>

- (void)clientTalkServiceDidReceiveAcceptTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)clientTalkServiceDidReceiveRejectTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)clientTalkServiceDidReceiveFinishTalkRequest:(id<CMClientTalkService>)clientTalkService;
- (void)clientTalkServiceDidReceiveBlockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService;
- (void)clientTalkServiceDidReceiveUnblockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService;

@end

@protocol CMClientTalkService <NSObject>

@property (nonatomic, weak) id<CMClientTalkServiceDelegate> delegate;

- (id)initWithConnectionAdapter:(id<CMConnectionAdapter>)connectionAdapter;

- (void)startVoiceChat;
- (void)stopVoiceChat;
- (void)disconnect;
- (void)unblockVoiceChat;
- (void)blockVoiceChat;
- (void)updateDedicatedHardwareAddress: (NSString *) address;

@end
