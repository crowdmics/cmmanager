//
//  CMDomainModel.h
//  CrowdMics
//
//  Created by Max Mashkov on 6/7/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import <DXDomain/DXDomain.h>

@interface CMDomainModel : DXDomainModel

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)serializedRepresentation;
+ (NSDictionary *)mapping;

@end
