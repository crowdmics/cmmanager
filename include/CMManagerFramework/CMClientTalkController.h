//
//  CMClientTalkController.h
//  CrowdMics
//
//  Created by Max Mashkov on 6/12/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMClientTalkService.h"

@interface CMClientTalkController : NSObject <CMClientTalkService>

@end
