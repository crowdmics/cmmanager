//
//  CMUser.h
//  CrowdMicsPrototype
//
//  Created by Maxim on 3/13/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMDomainModel.h"

typedef enum {
    UserStateNormal,
    UserStateTalk,
} UserState;

extern int kDefaultCrowdLimit;

@interface CMUser : CMDomainModel

@property (nonatomic, copy) NSNumber *ID;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSNumber *crowdLimitAmount;
@property (nonatomic, copy) NSDictionary *subscription;

@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, assign) UserState userState;

@property (strong, nonatomic, readonly) NSString *uuid;
@property (nonatomic, strong, readonly) NSString *fullName;


@property (nonatomic, assign, readonly) BOOL isAuthorized;

- (NSString*)serializedAvatar;

- (NSData*)avatarData;

- (void)setImageFromBase64String:(NSString*)base64String;

@end
