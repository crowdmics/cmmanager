//
// Created by zen on 6/5/13.
// Copyright (c) 2013 111Minutes. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "CMProtocolController.h"

@class CMEvent;


@interface CMClientProtocolController : CMProtocolController

@property(nonatomic, copy, readonly) NSString *userName;

- (void)connectWithCallback:(void (^)(BOOL success, NSError *error))callback;

@end